require 'socket'
load 'my_tools.rb'

class Email
	def self.pop3Error socket
		puts "CONNECTION ERROR!"
		socket.puts "QUIT"
		socket.gets
	end

	def self.pop3PutSubject socket, id
		socket.puts "RETR #{id}"
		puts "Mail ID: #{id}"
		while line = socket.gets
			token = line.split(':')[0]
			puts line if token == "Subject" || token == "From"
			break if line.delete("\r\n") == "."
		end
	end

	def self.listMailbox socket
		socket.puts "STAT"
		line = socket.gets.split
		if line[0]=="+OK"
			mail_num = line[1].to_i
			1.upto(mail_num) {|id|
				pop3PutSubject socket, id
			}
		else
			pop3Error socket
		end
	end

	def self.retrieveMail socket, id
		Msg.pop3_putMsg(socket, "RETR #{id}", true)
	end

	def self.receiver
		socket = TCPSocket.new User.host, 'pop3'
		puts socket.gets
		Msg.pop3_putMsg(socket, "USER #{User.login}")
		Msg.pop3_putMsg(socket, "PASS #{User.passwd}")
		while true
			puts "1 -- List Mails;\n2 -- Read Content;\nelse -- Quit."
			o = gets.to_i
			if o == 1
				listMailbox socket
			elsif o == 2
				puts "Input ID:"
				id = gets.to_i
				retrieveMail socket, id
			else
				break
			end
		end
		socket.puts "QUIT"
	end
end

