require 'socket'
require 'base64'
load 'my_tools.rb'
load 'my_email.rb'

class Email
	def sendEmail
		socket = TCPSocket.new(User.host, 'smtp')
		Msg.smtp_putMsg socket
		Msg.smtp_putMsg(socket, "EHLO localhost")
		Msg.smtp_putMsg_base64(socket, "AUTH LOGIN")
		Msg.smtp_putMsg_base64(socket, Base64.encode64(User.login).delete("\n"))
		Msg.smtp_putMsg(socket, Base64.encode64(User.passwd).delete("\n"))
		Msg.smtp_putMsg(socket, "MAIL FROM:<#{User.addr}>")
		Msg.smtp_putMsg(socket, "RCPT TO:<#{@to}>")
		Msg.smtp_putMsg(socket, "DATA")

		socket.puts "Subject: #{@subject}"
		socket.puts
		socket.puts @content

		Msg.smtp_putMsg(socket, ".")
		Msg.smtp_putMsg(socket, "QUIT")
	end
end

