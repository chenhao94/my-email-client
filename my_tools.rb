require 'socket'
require 'base64'

class User
	@@login, @@passwd, @@addr, @@host = "" 

	def self.getInfo filename
		info = File.new(filename,"r")
		@@addr = info.gets.delete "\n"
		@@passwd = info.gets.delete "\n"
		@@host = info.gets.delete "\n"
		@@login = @@addr.split('@')[0]
	end

	def self.login
		@@login
	end

	def self.passwd
		@@passwd
	end

	def self.addr
		@@addr
	end

	def self.host
		@@host
	end
end

class Msg 
	def self.smtp_putMsg(socket, msg="")
		socket.puts msg if msg!=""
		while line = socket.gets
			puts line
			break if line[3] == ' '
		end
	end

	def self.smtp_putMsg_base64(socket, msg="")
		socket.puts msg if msg!=""
		while line = socket.gets
			breakFlag = (line[3] == ' ')
			msgs = line.split(/[ -]/)
			puts msgs.delete_at(0)
			msgs.each {|msg|
				puts Base64.decode64(msg)
			}
			break if breakFlag
		end
	end

	def self.pop3_putMsg(socket, msg="", multiline=false)
		socket.puts msg if msg != ""
		if (!multiline)
			puts socket.gets
		else
			while line = socket.gets
				puts line
				break if line.delete("\r\n") == "."
			end
		end
	end
end

