class Email
	attr_accessor :subject, :content, :to

	def initialize(_to, _sub, _con)
		@to = _to
		@subject = _sub
		@content = _con
	end
end
