#My Email Client

##Introduction
This is a simple email client written in Ruby, which support simple SMTP and POP3 protocols. You can use it to do basic email operations, send and receive. Every emails I send and receive should *NOT* be encoded, since it is not the part I concern.

##Preliminaries
You should install the following software(s) before you can run this client:

* Ruby

And I highly recommand that you should use it under GNU/Linux, otherwise you cannot use our running script and have to write your own instead.

##Usage (Under GNU/Linux)
You should configure your email account in `user.config` before you can run it. It contains 3 lines: email address, password and mail server. Then you can run it by 
	
	./run.sh

Then follow the instructions given by this client.

##Implementations
No matter it wants to send or receive mails, it needs set up a connection first. I use the class `TCPSocket` of Ruby Standard Library to set up that.
###Sending (SMTP)
Once the connection set up, it sends `EHLO` along with the IP address. Then it sends `AUTH LOGIN` for authentication. The login name and the password should be encoded into Base64. Then we use `MAIL FROM` and `RCPT TO` to specify the sender and receiver's address. Next, it sends `DATA` to start the content of the email. We can send a line start with `Subject: ` to set the subject. The email ends up with a single line `.`. Finally, it sends a `QUIT` to finish the connection.
###Receiving (POP3)
First I use `USER` and `PASS` for authentication. If the user needs to read a certain mail, we use `RETR` command. Otherwise we can list all the mails in the mailbox. First I use `STAT` to figure out the number of mails. Then I retrieve all the mails and keep their subjects and senders only. Finally I list the mail IDs, subjects and senders, then the user can decide what to read. In the end, it sends a `QUIT` to tear down the connection.
