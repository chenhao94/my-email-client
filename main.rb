load 'my_tools.rb'
load 'my_email.rb'
load 'my_smtp.rb'
load 'my_pop3.rb'

User.getInfo "user.config"
while true
	puts "1 -- Send;\n2 -- Receive;\nelse -- Quit."
	o = gets.to_i
	if o == 1
		puts "Subject: "
		subject = gets.delete("\n")
		puts "To: "
		to = gets.delete("\n")
		puts "Content: "
		content = gets.delete("\n")
		Email.new(to, subject, content).sendEmail
	elsif o == 2
		Email.receiver
	else
		break
	end
end
